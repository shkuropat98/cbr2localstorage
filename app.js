const liveServer = require('live-server');
 
const params = {
    port: 3000,
    root: './public',
    mount: [['/components', './node_modules']],
};

liveServer.start(params);