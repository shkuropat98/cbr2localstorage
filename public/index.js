import { Archive } from './components/libarchive.js/main.js'

Archive.init({
  workerUrl: './components/libarchive.js/dist/worker-bundle.js'
})

const fileInput = document.getElementById('fileInput')
const resetButton = document.getElementById('resetButton')
const cont = document.getElementById('cont')

let dataBase = null

function addFilesToDb(data) {
  const transaction = dataBase.result.transaction(['book'], 'readwrite')
  const objectStore = transaction.objectStore('book')
  data.forEach(el => {
    const request = objectStore.add(el)
    request.onerror = (e) => console.error(request.error.message, e)
  })
  transaction.oncomplete = function (e) {
    console.log('Object successfully added')
    displayData()
  }
}

function displayData() {
  cont.innerHTML = 'extracting from database'
  const fragment = document.createDocumentFragment()
  const objectStore = dataBase.result.transaction('book').objectStore('book')
  objectStore.openCursor().onsuccess = function (event) {
    const cursor = event.target.result
    if (!cursor) {
      cont.innerHTML = ''
      cont.appendChild(fragment)
      return
    }
    const img = new Image()
    img.src = URL.createObjectURL(cursor.value.blob)
    img.alt = cursor.value.name
    img.title = cursor.value.name
    fragment.appendChild(img)
    cursor.continue()
  }
}

function startDB() {
  dataBase = window.indexedDB.open('archive', 1)
  dataBase.onupgradeneeded = () => {
    const objectStore = dataBase.result.createObjectStore('book', { keyPath: 'id', autoIncrement: true })
    objectStore.createIndex('name', 'name', { unique: false })
    objectStore.createIndex('blob', 'blob', { unique: false })
  }
  dataBase.onsuccess = () => {
    console.log('Database loaded')
    displayData()
  }
  dataBase.onerror = (e) => console.error('Error loading database', e)
}

function clearDB() {
  const transaction = dataBase.result.transaction(['book'], 'readwrite')
  const objectStore = transaction.objectStore('book')
  const request = objectStore.clear()
  request.onsuccess = displayData
  transaction.oncomplete = () => console.log('database cleared')
  transaction.onerror = (e) => console.log(transaction.error, e)
}

function registerFileInput() {
  fileInput.addEventListener('change', async (e) => {
    cont.innerHTML = 'uploading'
    const archive = await Archive.open(e.currentTarget.files[0])
    const obj = await archive.extractFiles()
    console.log(obj)
    const filesArr = Object.keys(obj).map(key => ({
      name: obj[key].name,
      blob: obj[key]
    }))
    addFilesToDb(filesArr)
  })
}

function registerResetButton() {
  resetButton.addEventListener('click', clearDB)
}

window.onload = function () {
  registerFileInput()
  registerResetButton()
  startDB()
}